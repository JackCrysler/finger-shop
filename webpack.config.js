var path = require('path')
var webpack = require('webpack')

module.exports = {
  entry: {//可以是字符，数组，对象
    'bundle.b':['./back/main.js'],
    //'bundle.f':['./back/main.js']
  },
  output: {
    path: path.resolve(__dirname, './dist'),//打包文件的输出目录
    publicPath: '/dist/', //公共资源的引用路径
    filename: '[name].js' //文件名
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          'vue-style-loader',//把js中的css字符提取出来追加到document文档的头部style标签中
          'css-loader'//把.vue文件中的style获取出来存在js包中
        ],
      },
      {
        test: /\.scss$/,
        use: [
          'vue-style-loader',
          'css-loader',
          'sass-loader'
        ],
      },
      {
        test: /\.vue$/,
        loader: 'vue-loader',//把.vue文件中的template，script，style提取出来分别处理
        options: {
          loaders: {
            // Since sass-loader (weirdly) has SCSS as its default parse mode, we map
            // the "scss" and "sass" values for the lang attribute to the right configs here.
            // other preprocessors should work out of the box, no loader config like this necessary.
            'scss': [
              'vue-style-loader',
              'css-loader',
              'sass-loader'
            ]
          }
          // other vue-loader options go here
        }
      },
      {
        test: /\.js$/,
        loader: 'babel-loader',//用babel-loader处理ES6，babel-loader会引用babel-core...执行编译
        exclude: /node_modules/
      },
      {
        test: /\.(png|jpg|gif|svg)$/,
        loader: 'file-loader',
        options: {
          name: '[name].[ext]?[hash]'
        }
      }
    ]
  },
  resolve: {
    alias: {
      'vue$': 'vue/dist/vue.esm.js',//重新配置引用的vue版本
      "common": __dirname+'/common'
    },
    extensions: ['*', '.js', '.vue', '.json']
  },
  devServer: {
    historyApiFallback: true,//路由启用history模式时，刷新时阻止页面报404
    noInfo: true,
    overlay: true
  },
  performance: {
    hints: false
  },
  plugins:[
    
  ],
  devtool: '#eval-source-map'//map映射，错误信息的源文件位置
}
//
if (process.env.NODE_ENV === 'production') {
  module.exports.devtool = '#source-map'
  // http://vue-loader.vuejs.org/en/workflow/production.html
  module.exports.plugins = (module.exports.plugins || []).concat([
    new webpack.DefinePlugin({//webpack主动向包中追加变量
      'process.env': {
        NODE_ENV: '"production"'
      }
    }),
    new webpack.optimize.UglifyJsPlugin({
      sourceMap: true,
      compress: {
        warnings: false
      }
    }),
    new webpack.LoaderOptionsPlugin({//兼容历史版本的webpack，起到polyfill的效果
      minimize: true
    })
  ])
}
