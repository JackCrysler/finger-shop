import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)
import {instance as http} from '../utils/http'
import * as MintUI from 'mint-ui'

let  {Indicator,MessageBox} = MintUI
let store = new Vuex.Store({
    state:{
        from:'',
        goodslist:[],
        checkedAll:false
    },
    mutations:{
        updateFrom(state,payload){
            state.from = payload
        },
        saveGoodslist(state,payload){
            state.goodslist = payload;
            
        },
        toggleChecked(state,payload){
            let arr = state.goodslist.map(item=>{
                if(item.id === payload[0]){
                    item.checked = payload[1]
                }
                return item
            })
            let isCheckedAll = arr.some(item=>{
                return item.checked===false
            })
            state.checkedAll = !isCheckedAll
            state.goodslist = arr
        },
        checkedAllFn(state){
            let ca = state.checkedAll
            let arr = state.goodslist.map(item=>{
                item.checked = ca
                return item
            })
            state.goodslist = arr
        }
    },
    actions:{
        getGoodsList({commit},payload){
            Indicator.open();
            http.get("/back/goodslist").then(res => {
                if (res.code == 0) {
                    let arr = res.goodslist.map(item=>{
                        item.checked = false
                        return item
                    })
                    localStorage.setItem('goodslist',JSON.stringify(arr));
                    commit('saveGoodslist',arr)
                }
                Indicator.close()
            })
        },
        onSale({},payload){
            Indicator.open();
            http.post('/back/onsale',payload).then(res=>{
                console.log(res)
                Indicator.close()
                if(res.code==0){
                    MessageBox.alert('操作成功')
                }
            })
        }
    }
})

export default store